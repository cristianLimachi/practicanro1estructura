/*
 * Realizar un programa que te realicela suma, la resta, la división y la multiplicación
de dos números pedidos por pantalla y el resultado debe ser mostrado por pantalla
 */
package PracticaNro1;

import java.util.Scanner;


public class EjercicioNro1 {

   public static void main(String[] args) {
       Scanner entrada=new Scanner(System.in);
       double Numero1=0;
       double Numero2=0;
       double Suma=0;
       double Resta=0;
       double Multiplicacion=0;
       double Division=0;
       
       System.out.println("Dame el primer numero");
       Numero1=entrada.nextDouble();
       System.out.println("Dame el segundo numero");
       Numero2=entrada.nextDouble();
       
       Suma=Numero1+Numero2;
       Resta=Numero1-Numero2;
       Multiplicacion=Numero1*Numero2;
       Division=Numero1/Numero2;
       
       
       System.out.println("La suma es:"+Suma);
       System.out.println("La resta es :"+Resta);
       System.out.println("La multiplicacion es:"+Multiplicacion);
       System.out.println("La division es:"+Division);
   }
}
   
    