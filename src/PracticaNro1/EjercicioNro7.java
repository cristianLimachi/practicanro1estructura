/*
 * Realizar un programa que cuente el número de cifras de un número entero positivo
(hay que controlarlo) pedido por teclado. Crea un método que realice esta acción,
pasando el número por parámetro, devolverá el número de cifras.
 */
package PracticaNro1;

import java.util.*;
import java.io.*;

public class EjercicioNro7 {
    public static void main(String[] args) throws IOException{
        Scanner sc = new Scanner(System.in);
        int n, cifras;
        char car;
        do{
            System.out.print("Introduce un número entero: ");
            n = sc.nextInt();
            cifras= 0;    
            while(n!=0){            
                    n = n/10;        
                   cifras++;         
            }
            System.out.println("El número tiene " + cifras+ " cifras");
            System.out.print("Continuar? ");
            car = (char)System.in.read();
        }while(car!='n' && car != 'N');   
    }
}