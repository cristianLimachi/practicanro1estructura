/*
 * Realizar un programa que nos convierta una cantidad de Bolivianos introducida por
teclado a otra moneda, estas pueden ser a dolares, euros o libras. El método tendrá
como parámetros, la cantidad de bolivianos y la moneda a pasar que sera una
cadena, este no devolverá ningún valor, mostrara un mensaje indicando el cambio.
 */
package PracticaNro1;


import java.util.Scanner;


public class EjercicioNro8 {

	
	public static void main(String[] args) {
		
		
		try {
			float bolivianos;
			String divisa;
			Scanner sc = new Scanner(System.in);
			System.out.println("Ingrese la cantidad de monedas en bolivianos");
			bolivianos=sc.nextFloat();
			System.out.println("Escriba el tipo de divisa a utilizar");
			System.out.println("1) dolares, 2) euros, 3) libras");
			divisa=sc.next();
			conversion(bolivianos, divisa);
			
		} catch (java.util.InputMismatchException e) {
			System.out.println("Debe ingresar un numero");
		}

	}
	
	
	public static double conversion(float bolivianos, String divisa) {
		double resultado=0;
		boolean correcto=true;
		switch (divisa) {
		case "dolares":
			resultado= bolivianos*0.14;
			break;
		case "euros":
			resultado= bolivianos*0.13;
			break;
		case "libras":
			resultado= bolivianos*0.11;
			break;
		default:
			correcto= false;
			break;
		}
		
		
		
		if (correcto) {
			System.out.println(bolivianos + " bolivianos convertidos a " + divisa + " son " + resultado +" "+ divisa );
		}else {
			System.out.println("el tipo de divisa es incorrecto, debe escribir la palabra ");
			System.out.println("dolares o  euros o libras");
			
		}
		
		return 0;
	}

}