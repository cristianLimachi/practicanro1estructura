/*
 * Realizar unprograma que lea un carácter por teclado y compruebe si es una letra
mayúscula
 */
package PracticaNro1;

import java.io.*;

public class EjercicioNro4{
    
public static void main(String[] args) throws IOException {
        char car1, car2;
        System.out.println("Introduzca primer carácter: ");
        car1 = (char)System.in.read();
        System.in.read(); 
        System.out.println("Introduzca segundo carácter: ");
        car2 = (char)System.in.read();    
        if(Character.isLowerCase(car1)){       
           if(Character.isLowerCase(car2)){
               System.out.println("Los dos son letras minúsculas");
           }else{
               System.out.println("El primero es una letra minúscula pero el segundo no");
           }
        }else if(Character.isLowerCase(car2)){
                 System.out.println("El segundo es una letra minúscula pero el primero no");
               }else{
                 System.out.println("Ninguno es una letra minúscula");
               }
        }
   }
