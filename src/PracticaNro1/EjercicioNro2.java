/*
 * Realizar un programa que te realice el promedio de las notas de un alumno, para
ello el programa te va a tener que solicitar el nombre del alumno y las notas de las
3 evaluaciones.
 */
package PracticaNro1;


import java.util.*;
public class EjercicioNro2{
public static void main(String[]args){
    
Scanner neme=new Scanner(System.in);
String nom;

System.out.println("ingrese el nombre del alumno");
nom=neme.next();

int ncal;
System.out.println("ingrese el numero de calificaciones");
ncal=neme.nextInt();

double i=1.0,prom=0.0,suma=0.0;

while(i>=1 && i<=ncal){
double cal;
System.out.println("ingrese la calificacion");
cal=neme.nextDouble();
suma=suma+cal;
i++;
}
prom=suma/ncal;
System.out.println("el promedio del alumno"+nom+"es: "+prom);

}
}