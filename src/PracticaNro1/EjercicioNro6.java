/*
 * Realizar un programa que nos convierta un número en base decimal a binario.
Esto lo realizara un método al que le pasaremos el numero como parámetro,
devolverá un String con el numero convertido a binario. Para convertir un numero
decimal a binario, debemos dividir entre 2 el numero y el resultado de esa división
se divide entre 2 de nuevo hasta que no se pueda dividir mas, el resto que
obtengamos de cada división formara el numero binario, de abajo a arriba.
 */
package PracticaNro1;

import java.util.Scanner;


public class EjercicioNro6{

    public static void main(String[] args) {

        int numero, exp, digito;
        double binario;
        Scanner sc = new Scanner(System.in);

        do{ 
            System.out.print("Introduce un numero entero >= 0: ");
            numero = sc.nextInt();
        }while(numero<0);

        exp=0;
        binario=0;
        while(numero!=0){
                digito = numero % 2;           
                binario = binario + digito * Math.pow(10, exp);  
                exp++;
                numero = numero/2;
        }
        System.out.printf("Binario: %.0f %n", binario);
    }
}
